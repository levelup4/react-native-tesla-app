import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  carContainer: {
    width: "100%",
    height: "100%"
  },
  backgroundImage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  titles: {
    marginTop: "30%",
    width: "100%",
    alignItems: "center"
  },
  title: {
    fontSize: 40,
    fontWeight: "500"
  },
  subtitle: {
    fontSize: 16,
    color: "#5c5e62"
  }
});

export default styles;
