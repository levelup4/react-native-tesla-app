import React from "react";
import { View, Text, ImageBackground } from "react-native";
import styles from "./styles";

function CarItem() {
  return (
    <View style={styles.carContainer}>
      <ImageBackground
        style={styles.backgroundImage}
        source={require("../../assets/images/ModelS.jpeg")}
      />
      <View style={styles.titles}>
        <Text style={styles.title}>Model S</Text>
        <Text style={styles.subtitle}>Starting at $69,420</Text>
      </View>
    </View>
  );
}

export default CarItem;
